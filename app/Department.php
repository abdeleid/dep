<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use Sluggable;

    use \Dimsav\Translatable\Translatable;

    protected $guarded = [];
    public $translatedAttributes = ['name'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];

    }//end fo sluggable

    public function getRouteKeyName()
    {
        return 'slug';

    }//end of get route key name

    public function departments()
    {
        return $this->hasMany(Department::class, 'parent_id', 'id');

    }//end of department

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_department');

    }//end of user

    public function pages()
    {
        return $this->hasMany(Page::class);

    }//end of pages

    public function faqs()
    {
        return $this->hasMany(Faq::class);

    }//end of pages

    public function news()
    {
        return $this->hasMany(News::class);

    }//end of news

    public function events()
    {
        return $this->hasMany(Event::class);

    }//end of news

    public function galleries()
    {
        return $this->hasMany(Gallery::class);

    }//end of galleries

    public function videos()
    {
        return $this->hasMany(Video::class);

    }//end of galleries

}//end of model
