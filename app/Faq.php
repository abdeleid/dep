<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    use \Dimsav\Translatable\Translatable;

    protected $guarded = [];
    public $translatedAttributes = ['question', 'answer'];

    protected $appends = ['image_path'];

    public function getImagePathAttribute()
    {
        return asset('uploads/faq_images/' . $this->image);

    }//end of get image path

}//end of model
