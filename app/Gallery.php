<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    use \Dimsav\Translatable\Translatable;

    protected $guarded = [];
    public $translatedAttributes = ['title'];

    public function gallery_images()
    {
        return $this->morphMany(GalleryImage::class, 'galleryable')->orderBy('order', 'asc');

    }//end of gallery images

}//end of model
