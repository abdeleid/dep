<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GalleryImage extends Model
{
    protected $guarded = ['id'];

    public function galleryable()
    {
        return $this->morphTo();

    }//end of galleryable

}//end of model
