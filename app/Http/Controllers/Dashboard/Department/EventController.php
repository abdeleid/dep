<?php

namespace App\Http\Controllers\Dashboard\Department;

use App\Department;
use App\Event;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class EventController extends Controller
{
    public function __construct()
    {
        $this->middleware('department_member');
        $this->middleware('permission:read_department_events')->only('index');
        $this->middleware('permission:create_department_events')->only('create');
        $this->middleware('permission:update_department_events')->only('edit');
        $this->middleware('permission:delete_department_events')->only('destroy');

    }//end of construct

    public function index(Request $request, Department $department)
    {
        $events = $department->events()->where(function ($q) use ($request) {

            return $q->when($request->search, function ($query) use ($request) {
                return $query->whereTranslationLike('title', $request->search);
            });

        })->latest()->paginate(5);

        return view('dashboard.departments.events.index', compact('department', 'events'));

    }//end of index

    public function create(Department $department)
    {
        return view('dashboard.departments.events.create', compact('department'));

    }//end fo create

    public function store(Request $request, Department $department)
    {
        $rules = [];

        foreach (config('translatable.locales') as $locale) {

            $rules += [$locale . '.title' => 'required|unique:page_translations,title'];
            $rules += [$locale . '.content' => 'required'];

        } //end of for each

        $request->validate($rules);

        $request_data = $request->all();

        if ($request->image) {

            $request->file('image')->store('/uploads', 'public_uploads');
            $request_data['image'] = $request->image->hashName();

        }//end of if

        $department->events()->create($request_data);
        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('dashboard.departments.events.index', $department->slug);

    }//end of store

    public function edit(Department $department, Event $event)
    {
        return view('dashboard.departments.events.edit', compact('department', 'event'));

    }//end of edit

    public function update(Request $request, Department $department, Event $event)
    {
        $rules = [];

        foreach (config('translatable.locales') as $locale) {

            $rules += [$locale . '.title' => ['required', Rule::unique('event_translations', 'title')->ignore($event->id, 'event_id')]];
            $rules += [$locale . '.content' => 'required'];

        } //end of for each

        $request->validate($rules);

        $request_data = $request->all();

        if ($request->image) {

            if ($event->image) {

                Storage::disk('public_uploads')->delete('/uploads/' . $event->image);

            }//end of inf

            $request->file('image')->store('/uploads', 'public_uploads');
            $request_data['image'] = $request->image->hashName();

        }//end of if

        $event->update($request_data);
        session()->flash('success', __('site.updated_successfully'));
        return redirect()->route('dashboard.departments.events.index', $department->slug);

    }//end of update

    public function destroy(Department $department, Event $event)
    {
        if ($event->image) {

            Storage::disk('public_uploads')->delete('/uploads/' . $event->image);

        }//end of if

        $event->delete();
        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->route('dashboard.departments.events.index', $department->slug);

    }//end fo destroy

}//end of controller
