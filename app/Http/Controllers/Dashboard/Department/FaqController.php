<?php

namespace App\Http\Controllers\Dashboard\Department;

use App\Department;
use App\Faq;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;

class FaqController extends Controller
{
    public function __construct()
    {
        $this->middleware('department_member');
        $this->middleware('permission:read_department_faqs')->only('index');
        $this->middleware('permission:create_department_faqs')->only('create');
        $this->middleware('permission:update_department_faqs')->only('edit');
        $this->middleware('permission:delete_department_faqs')->only('destroy');

    }//end of construct

    public function index(Request $request, Department $department)
    {
        $faqs = $department->faqs()->when($request->search, function ($q) use ($request) {

            return $q->whereTranslationLike('title', '%' . $request->search . '%')
                ->orWhereTranslationLike('content', '%' . $request->search . '%');

        })->when($request->status, function ($q) use ($request) {

            return $q->where('status', $request->status);

        })->latest()->paginate(5);

        return view('dashboard.departments.faqs.index', compact('department', 'faqs'));

    }//end of index

    public function create(Department $department)
    {
        return view('dashboard.departments.faqs.create', compact('department'));

    }//end of create

    public function store(Request $request, Department $department)
    {
        $rules = [];

        foreach (config('translatable.locales') as $locale) {

            $rules += [$locale . '.question' => 'required|unique:faq_translations,question'];
            $rules += [$locale . '.answer' => 'required'];

        } //end of for each

        $request->validate($rules);

        $request_data = $request->all();

        if ($request->image) {

            $request->file('image')->store('/uploads', 'public_uploads');
            $request_data['image'] = $request->image->hashName();

        }//end of if

        $department->faqs()->create($request_data);
        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('dashboard.departments.faqs.index', $department->slug);

    }//end of store

    public function edit(Department $department, Faq $faq)
    {
        return view('dashboard.departments.faqs.edit', compact('department', 'faq'));

    }//end of edit

    public function update(Request $request, Department $department, Faq $faq)
    {
        $rules = [];

        foreach (config('translatable.locales') as $locale) {

            $rules += [$locale . '.question' => ['required', Rule::unique('faq_translations', 'question')->ignore($faq->id, 'faq_id')]];
            $rules += [$locale . '.answer' => ['required']];

        } //end of for each

        $request->validate($rules);

        $request_data = $request->all();

        if ($request->image) {

            if ($faq->image) {

                Storage::disk('public_uploads')->delete('/uploads/' . $faq->image);

            }//end of inf

            $request->file('image')->store('/uploads', 'public_uploads');
            $request_data['image'] = $request->image->hashName();

        }//end of if

        $request_data['department_id'] = $department->id;

        $faq->update($request_data);
        session()->flash('success', __('site.updated_successfully'));
        return redirect()->route('dashboard.departments.faqs.index', $department->slug);

    }//end of update

    public function destroy(Department $department, Faq $faq)
    {
        if ($faq->image) {

            Storage::disk('public_uploads')->delete('/uploads/' . $faq->image);

        }//end of if

        $faq->delete();
        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->route('dashboard.departments.faqs.index', $department->slug);

    }//end of destroy

}//end of controller
