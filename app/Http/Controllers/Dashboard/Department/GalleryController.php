<?php

namespace App\Http\Controllers\Dashboard\Department;

use App\Department;
use App\Gallery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;

class GalleryController extends Controller
{
    public function __construct()
    {
        $this->middleware('department_member');
        $this->middleware('permission:read_department_galleries')->only('index');
        $this->middleware('permission:create_department_galleries')->only('create');
        $this->middleware('permission:update_department_galleries')->only('edit');
        $this->middleware('permission:delete_department_galleries')->only('destroy');

    }//end of construct

    public function index(Request $request, Department $department)
    {
        $galleries = $department->galleries()->paginate(5);
        return view('dashboard.departments.galleries.index', compact('department', 'galleries'));

    }//end of index

    public function create(Department $department)
    {
        return view('dashboard.departments.galleries.create', compact('department'));

    }//end fo create

    public function store(Request $request, Department $department)
    {
        $rules = [];

        foreach (config('translatable.locales') as $locale) {
            $rules += [$locale . '.title' => ['required', Rule::unique('gallery_translations', 'title')]];

        }

        $request->validate($rules);
        $department->galleries()->create($request->all());
        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('dashboard.departments.galleries.index', $department->slug);

    }//end fo store

    public function edit(Department $department, Gallery $gallery)
    {
        return view('dashboard.departments.galleries.edit', compact('department', 'gallery'));

    }//end of edit

    public function update(Request $request, Department $department, Gallery $gallery)
    {
        $rules = [];

        foreach (config('translatable.locales') as $locale) {

            $rules += [$locale . '.title' => ['required', Rule::unique('gallery_translations', 'title')->ignore($gallery->id, 'gallery_id')]];

        }//end of for each

        $request->validate($rules);

        $gallery->update($request->all());

        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('dashboard.departments.galleries.index', $department->slug);

    }//end of update

    public function upload_images(Request $request, Department $department, Gallery $gallery)
    {
        $request->file('file')->store('gallery_images', 'public_uploads');

        $gallery->images()->create([
            'name' => $request->file->hashName()
        ]);

    }//end of upload images

    public function delete(Department $department, Gallery $gallery)
    {
        $gallery->delete();
        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->route('dashboard.departments.galleries.index', $department->slug);

    }//end of delete

}//end of controller
