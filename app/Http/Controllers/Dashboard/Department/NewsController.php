<?php

namespace App\Http\Controllers\Dashboard\Department;

use App\Department;
use App\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;

class NewsController extends Controller
{
    public function __construct()
    {
        $this->middleware('department_member');
        $this->middleware('permission:read_department_news')->only('index');
        $this->middleware('permission:create_department_news')->only('create');
        $this->middleware('permission:update_department_news')->only('edit');
        $this->middleware('permission:delete_department_news')->only('destroy');

    }//end of construct

    public function index(Request $request, Department $department)
    {
        $news = $department->news()->where(function($q) use ($request) {

            return $q->when($request->search, function($query) use ($request) {

                return $query->whereTranslationLike('title', '%' . $request->search . '%');

            });

        })->latest()->paginate(5);

        return view('dashboard.departments.news.index', compact('department', 'news'));

    }//end of index

    public function create(Department $department)
    {
        return view('dashboard.departments.news.create', compact('department'));

    }//end fo create

    public function store(Request $request, Department $department, News $news)
    {
        $rules = [];

        foreach (config('translatable.locales') as $locale) {

            $rules += [$locale . '.*' => 'required'];
            $rules += [$locale . '.title' => ['required', Rule::unique('news_translations', 'title')]];

        }//end of for each

        $request->validate($rules);

        $department->news()->create($request->all());
        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('dashboard.departments.news.index', $department->slug);

    }//end of store

    public function edit(Department $department, News $news)
    {
        return view('dashboard.departments.news.edit', compact('department', 'news'));

    }//end of edit

    public function update(Request $request, Department $department, News $news)
    {
        $rules = [];

        foreach (config('translatable.locales') as $locale) {

            $rules += [$locale . '.*' => 'required'];
            $rules += [$locale . '.title' => ['required', Rule::unique('news_translations', 'title')->ignore($news->id, 'news_id')]];

        }//end of for each

        $request->validate($rules);

        $news->update($request->all());

        session()->flash('success', __('site.updated_successfully'));
        return redirect()->route('dashboard.departments.news.index', $department->slug);

    }//end of update

    public function destroy(Department $department, News $news)
    {
        $news->delete();
        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->route('dashboard.departments.news.index', $department->slug);

    }//end of destroy

}//end of controller
