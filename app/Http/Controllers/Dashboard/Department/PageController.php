<?php

namespace App\Http\Controllers\Dashboard\Department;

use App\Department;
use App\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class PageController extends Controller
{
    public function __construct()
    {
        $this->middleware('department_member');
        $this->middleware('permission:read_department_pages')->only('index');
        $this->middleware('permission:create_department_pages')->only('create');
        $this->middleware('permission:update_department_pages')->only('edit');
        $this->middleware('permission:update_department_pages')->only('remove_image');
        $this->middleware('permission:delete_department_pages')->only('destroy');

    }//end of construct

    public function index(Request $request, Department $department)
    {
        $pages = $department->pages()->when($request->search, function ($q) use ($request) {

            return $q->whereTranslationLike('title', '%' . $request->search . '%')
                ->orWhereTranslationLike('content', '%' . $request->search . '%');

        })->when($request->status, function ($q) use ($request) {

            return $q->where('status', $request->status);

        })->latest()->paginate(5);

        return view('dashboard.departments.pages.index', compact('department', 'pages'));

    }//end of index

    public function create(Department $department)
    {
        return view('dashboard.departments.pages.create', compact('department'));

    }//end of create

    public function store(Request $request, Department $department)
    {
        $rules = [];

        foreach (config('translatable.locales') as $locale) {

            $rules += [$locale . '.*' => 'required'];
            $rules += [$locale . '.title' => 'unique:page_translations,title'];

        } //end of for each

        $rules += [
            'status' => 'required',
            'comment_status' => 'required',
        ];

        $request->validate($rules);

        $request_data = $request->all();

        if ($request->image) {

            $request->file('image')->store('/uploads', 'public_uploads');
            $request_data['image'] = $request->image->hashName();

        }//end of if

        $department->pages()->create($request_data);
        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('dashboard.departments.pages.index', $department->slug);

    }//end of store

    public function edit(Department $department, Page $page)
    {
        return view('dashboard.departments.pages.edit', compact('department', 'page'));

    }//end of edit

    public function update(Request $request, Department $department, Page $page)
    {
        $rules = [];

        foreach (config('translatable.locales') as $locale) {

            $rules += [$locale . '.*' => 'required'];
            $rules += [$locale . '.title' => [Rule::unique('page_translations')->ignore($page->id, 'page_id')]];

        } //end of for each

        $rules += [
            'status' => 'required',
            'comment_status' => 'required',
        ];

        $request->validate($rules);

        $request_data = $request->all();

        if ($request->image) {

            if($page->image) {

                Storage::disk('public_uploads')->delete('/uploads/' . $page->image);

            }//end of inf

            $request->file('image')->store('page_images', 'public_uploads');
            $request_data['image'] = $request->image->hashName();

        }//end of if

        $page->update($request_data);
        session()->flash('success', __('site.updated_successfully'));
        return redirect()->route('dashboard.departments.pages.index', $department->slug);

    }//end of update
    
    public function remove_image(Request $request, Page $page)
    {
        Storage::disk('public_uploads')->delete('/uploads/' . $page->image);

        $page->update([
            'image' => null
        ]);

        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('dashboard.pages.edit', $page->id);
        
    }//end of remove image

    public function destroy(Department $department, Page $page)
    {
        if($page->image) {

            Storage::disk('public_uploads')->delete('/uploads/' . $page->image);

        }//end of if

        $page->delete();
        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->route('dashboard.departments.pages.index', $department->slug);

    }//end of destroy

}//end of controller
