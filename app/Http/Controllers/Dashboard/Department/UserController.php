<?php

namespace App\Http\Controllers\Dashboard\Department;

use App\Department;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('department_member');
        $this->middleware('permission:read_department_users')->only('index');
        $this->middleware('permission:create_department_users')->only('create');
        $this->middleware('permission:update_department_users')->only('edit');
        $this->middleware('permission:delete_department_users')->only('destroy');

    }//end of construct
    
    public function index(Request $request, Department $department)
    {
        $users = $department->users()->when($request->search, function($q) use ($request) {

            return $q->where('name', 'like', '%' . $request->search . '%')
                ->orWhere('email', 'like', '%' . $request->search . '%');

        })->whereRoleIs('admin')->latest()->paginate(5);

        return view('dashboard.departments.users.index', compact('department', 'users'));
    
    }//end of index
    
    public function create(Department $department)
    {
        return view('dashboard.departments.users.create', compact('department'));
        
    }//end of create
    
    public function store(Request $request, Department $department)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required|confirmed',
        ]);

        $request_data = $request->except(['password', 'permissions']);;
        $request_data['password'] = bcrypt($request->password);

        $user = User::create($request_data);

        $user->departments()->attach($department->id);
        $user->attachRole('admin');
        $user->syncPermissions($request->permissions);
        $user->attachPermission('read_departments');

        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('dashboard.departments.users.index', $department->slug);

    }//end of store
    
    public function edit(Department $department, User $user)
    {
        return view('dashboard.departmewnts.users.edit', compact('department', 'user'));
    
    }//end of edit
    
    public function update(Request $request, Department $department, User $user)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|unique:users,email,' . $user->id,
        ]);

        $user->update($request->all());
        $user->syncPermissions($request->permissions);

        session()->flash('success', __('site.updated_successfully'));
        return redirect()->route('dashboard.departments.users.index', $department->slug);

    }//end of update
    
    public function destroy(Department $department, User $user)
    {
        $user->delete();
        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->route('dashboard.departments.users.index', $department->slug);
    
    }//end of destroy

}//end of controller
