<?php

namespace App\Http\Controllers\Dashboard\Department;

use App\Department;
use App\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;

class VideoController extends Controller
{
    public function __construct()
    {
        $this->middleware('department_member');
        $this->middleware('permission:read_department_videos')->only('index');
        $this->middleware('permission:create_department_videos')->only('create');
        $this->middleware('permission:update_department_videos')->only('edit');
        $this->middleware('permission:delete_department_videos')->only('destroy');

    }//end of construct

    public function index(Request $request, Department $department)
    {
        $videos = $department->videos()->where(function ($q) use ($request) {

            return $q->when($request->search, function ($query) use ($request) {

                return $query->whereTranslationLike('title', '%' . $request->search . '%');

            });

        })->latest()->paginate(5);

        return view('dashboard.departments.videos.index', compact('department', 'videos'));

    }//end of index

    public function create(Department $department)
    {
        return view('dashboard.departments.videos.create', compact('department'));

    }//end fo create

    public function store(Request $request, Department $department)
    {
        $rules = [];

        foreach (config('translatable.locales') as $locale) {

            $rules += [$locale . '.title' => 'required|unique:video_translations,title'];

        } //end of for each

        $rules += [
            'youtube_url' => 'required'
        ];

        $request->validate($rules);

        $department->videos()->create($request->all());
        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('dashboard.departments.videos.index', $department->slug);

    }//end of store

    public function edit(Department $department, Video $video)
    {
        return view('dashboard.departments.videos.edit', compact('department', 'video'));

    }//end of edit

    public function update(Request $request, Department $department, Video $video)
    {
        $rules = [];

        foreach (config('translatable.locales') as $locale) {

            $rules += [$locale . '.title' => ['required', Rule::unique('video_translations', 'title')->ignore($video->id, 'video_id')]];

        } //end of for each

        $rules += [
            'youtube_url' => 'required'
        ];

        $request->validate($rules);

        $video->update($request->all());
        session()->flash('success', __('site.updated_successfully'));
        return redirect()->route('dashboard.departments.videos.index', $department->slug);

    }//end of update

    public function destroy(Department $department, Video $video)
    {
        $video->delete();
        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->route('dashboard.departments.videos.index', $department->slug);

    }//end fo destroy


}//end of controller
