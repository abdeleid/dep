<?php

namespace App\Http\Controllers\Dashboard;

use App\Department;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;

class DepartmentController extends Controller
{
    public function __construct()
    {
        if (request()->parent){

            $this->middleware(['permission:read_department_departments'])->only('index');
            $this->middleware(['permission:create_department_departments'])->only('create');
            $this->middleware(['permission:update_department_departments'])->only('edit');
            $this->middleware(['permission:delete_department_departments'])->only('destroy');

        } else {

            $this->middleware(['permission:read_departments'])->only('index');
            $this->middleware(['permission:create_departments'])->only('create');
            $this->middleware(['permission:update_departments'])->only('edit');
            $this->middleware(['permission:delete_departments'])->only('destroy');

        }//end of else

        $this->middleware(['department_member'])->only(['show', 'edit', 'update', 'delete']);

    }//end of constructor

    public function index(Request $request)
    {
        $departments = $this->get_departments($request);

        return view('dashboard.departments.index', compact('departments'));

    }//end of index

    public function create()
    {
        $users = User::all();
        return view('dashboard.departments.create', compact('users'));

    }//end of create

    public function store(Request $request)
    {
        $rules = [];

        foreach (config('translatable.locales') as $locale) {
            $rules += [$locale . '.name' => ['required', Rule::unique('department_translations', 'name')]];
        }

        $request->validate($rules);

        if ($request->parent) {

            $parent = Department::where('slug', $request->parent)->first();
            $department = $parent->departments()->create($request->except('parent'));

        } else {

            $department = Department::create($request->all());

        }//end of else

        auth()->user()->departments()->attach($department->id);
        session()->flash('success', __('site.added_successfully'));

        if ($request->parent) {
            return redirect()->route('dashboard.departments.index', ['parent' => $parent->slug]);
        }

        return redirect()->route('dashboard.departments.index');

    }//end of store

    public function show(Department $department)
    {
        return view('dashboard.departments.show', compact('department'));

    }//end of show

    public function edit(Department $department)
    {
        return view('dashboard.departments.edit', compact('department'));

    }//end of edit

    public function update(Request $request, Department $department)
    {
        $rules = [];

        foreach (config('translatable.locales') as $locale) {
            $rules += [$locale . '.*' => 'required'];
            $rules += [$locale . '.name' => [Rule::unique('department_translations')->ignore($department->id, 'department_id')]];
        }

        $request->validate($rules);

        $department->update($request->except(['parent']));
        session()->flash('success', __('site.updated_successfully'));

        if ($request->parent) {

            $parent = Department::where('slug', $request->parent)->first();
            return redirect()->route('dashboard.departments.index', ['parent' => $parent->slug]);

        }//end of if

        return redirect()->route('dashboard.departments.index');

    }//end of update

    public function destroy(Request $request, Department $department)
    {

        $department->delete();
        session()->flash('success', __('site.deleted_successfully'));

        if ($request->parent) {

            $parent = Department::where('slug', $request->parent)->first();
            return redirect()->route('dashboard.departments.index', ['parent' => $parent->slug]);

        }//end of if

        return redirect()->route('dashboard.departments.index');

    }//end of destroy

    private function get_departments($request) {

        //super admin shows all departments
        if (auth()->user()->hasRole('super_admin')) {

            if ($request->parent) {

                $parent = Department::where('slug', $request->parent)->first();

                $departments = $parent->departments()->where(function ($q) use ($request) {

                    return $q->when($request->search, function ($query) use ($request) {

                        return $query->whereTranslationLike('name', '%' . $request->search . '%');

                    });

                })->latest()->paginate(10);

            } else {

                $departments = Department::when($request->search, function ($q) use ($request) {

                    return $q->whereTranslationLike('name', '%' . $request->search . '%');

                })->where('parent_id', null)->latest()->paginate(10);

            }//end of else

        } else {

            //admins users show only admin departments
            if ($request->parent) {

                $parent = Department::where('slug', $request->parent)->first();

                $departments = $parent->departments()->where(function ($q) use ($request) {

                    return $q->when($request->search, function ($query) use ($request) {

                        return $query->whereTranslationLike('name', '%' . $request->search . '%');

                    });

                })->whereIn('id', auth()->user()->departments->pluck('id')->toArray())->latest()->paginate(10);

            } else {

                $departments = auth()->user()->departments()->when($request->search, function ($q) use ($request) {

                    return $q->whereTranslationLike('name', '%' . $request->search . '%');

                })->where('parent_id', null)->latest()->paginate(10);

            }//end of else

        }//end of else

        return $departments;

    }//end of departments

}//end of controller
