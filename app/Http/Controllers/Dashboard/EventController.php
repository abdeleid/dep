<?php

namespace App\Http\Controllers\Dashboard;

use App\Event;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class EventController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:read_events')->only('index');
        $this->middleware('permission:create_events')->only('create');
        $this->middleware('permission:update_events')->only('edit');
        $this->middleware('permission:delete_events')->only('destroy');

    }//end of constructor

    public function index(Request $request)
    {
        $events = Event::where('department_id', null)->where(function ($q) use ($request) {

            return $q->when($request->search, function ($query) use ($request) {

                return $query->whereTranslationLike('title', '%' . $request->search . '%')
                    ->orWhereTranslationLike('content', '%' . $request->search . '%');

            });

        })->latest()->paginate(5);

        return view('dashboard.events.index', compact('events'));

    }//end of index

    public function create()
    {
        return view('dashboard.events.create');

    }//end of create

    public function store(Request $request)
    {
        $rules = [];

        foreach (config('translatable.locales') as $locale) {

            $rules += [$locale . '.title' => 'required|unique:page_translations,title'];
            $rules += [$locale . '.content' => 'required'];

        } //end of for each

        $request->validate($rules);

        $request_data = $request->all();

        if ($request->image) {

            $request->file('image')->store('/uploads', 'public_uploads');
            $request_data['image'] = $request->image->hashName();

        }//end of if

        Event::create($request_data);
        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('dashboard.events.index');

    }//end of store

    public function edit(Event $event)
    {
        return view('dashboard.events.edit', compact('event'));

    }//end of edit

    public function update(Request $request, Event $event)
    {
        $rules = [];

        foreach (config('translatable.locales') as $locale) {

            $rules += [$locale . '.title' => ['required', Rule::unique('event_translations', 'title')->ignore($event->id, 'event_id')]];
            $rules += [$locale . '.content' => 'required'];

        } //end of for each

        $request->validate($rules);

        $request_data = $request->all();

        if ($request->image) {

            if ($event->image) {

                Storage::disk('public_uploads')->delete('/uploads/' . $event->image);

            }//end of inf

            $request->file('image')->store('/uploads', 'public_uploads');
            $request_data['image'] = $request->image->hashName();

        }//end of if

        $event->update($request_data);
        session()->flash('success', __('site.updated_successfully'));
        return redirect()->route('dashboard.events.index');

    }//end of update

    public function destroy(Event $event)
    {
        if ($event->image) {

            Storage::disk('public_uploads')->delete('uploads/' . $event->image);

        }//end of if

        $event->delete();
        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->route('dashboard.events.index');

    }//end of destroy

}//end of controller
