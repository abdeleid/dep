<?php

namespace App\Http\Controllers\Dashboard;

use App\Faq;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class FaqController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:read_faqs')->only('index');
        $this->middleware('permission:create_faqs')->only('create');
        $this->middleware('permission:update_faqs')->only('edit');
        $this->middleware('permission:delete_faqs')->only('destroy');

    }//end of constructor

    public function index(Request $request)
    {
        $faqs = Faq::where('department_id', null)->where(function ($q) use ($request) {

            return $q->when($request->search, function ($query) use ($request) {

                return $query->whereTranslationLike('question', '%' . $request->search . '%')
                    ->orWhereTranslationLike('answer', '%' . $request->search . '%');

            });

        })->latest()->paginate(5);

        return view('dashboard.faqs.index', compact('faqs'));

    }//end of index

    public function create()
    {
        return view('dashboard.faqs.create');

    }//end of create

    public function store(Request $request)
    {
        $rules = [];

        foreach (config('translatable.locales') as $locale) {

            $rules += [$locale . '.question' => 'required|unique:faq_translations,question'];
            $rules += [$locale . '.answer' => 'required'];

        } //end of for each

        $request->validate($rules);

        $request_data = $request->all();

        if ($request->image) {

            $request->file('image')->store('/uploads', 'public_uploads');
            $request_data['image'] = $request->image->hashName();

        }//end of if

        Faq::create($request_data);
        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('dashboard.faqs.index');

    }//end of store

    public function edit(Faq $faq)
    {
        return view('dashboard.faqs.edit', compact('faq'));

    }//end of edit

    public function update(Request $request, Faq $faq)
    {
        $rules = [];

        foreach (config('translatable.locales') as $locale) {

            $rules += [$locale . '.question' => ['required', Rule::unique('faq_translations', 'question')->ignore($faq->id, 'faq_id')]];
            $rules += [$locale . '.answer' => ['required']];

        } //end of for each

        $request->validate($rules);

        $request_data = $request->all();

        if ($request->image) {

            if ($faq->image) {

                Storage::disk('public_uploads')->delete('/uploads/' . $faq->image);

            }//end of inf

            $request->file('image')->store('/uploads', 'public_uploads');
            $request_data['image'] = $request->image->hashName();

        }//end of if

        $faq->update($request_data);
        session()->flash('success', __('site.updated_successfully'));
        return redirect()->route('dashboard.faqs.index');

    }//end of update

    public function destroy(Faq $faq)
    {
        if ($faq->image) {

            Storage::disk('public_uploads')->delete('/uploads/' . $faq->image);

        }//end of if

        $faq->delete();
        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->route('dashboard.faqs.index');

    }//end of destroy

}//end of controller
