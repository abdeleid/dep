<?php

namespace App\Http\Controllers\Dashboard;

use App\Gallery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class GalleryController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:read_galleries')->only('index');
        $this->middleware('permission:create_galleries')->only('create');
        $this->middleware('permission:update_galleries')->only('edit');
        $this->middleware('permission:delete_galleries')->only('destroy');

    }//end of constructor

    public function index(Request $request)
    {

        $galleries = Gallery::where('department_id', null)->where(function ($q) use ($request) {

            return $q->when($request->search, function ($query) use ($request) {

                return $query->whereTranslationLike('title', '%' . $request->search . '%');

            })->when($request->status, function ($query) use ($request) {

                return $query->where('status', $request->status);

            });

        })->latest()->paginate(5);

        return view('dashboard.galleries.index', compact('galleries'));

    }//end of index

    public function create()
    {
        return view('dashboard.galleries.create');

    }//end of create

    public function store(Request $request)
    {
        $rules = [];

        foreach (config('translatable.locales') as $locale) {

            $rules += [$locale . '.title' => 'unique:gallery_translations,title'];

        } //end of for each

        $request->validate($rules);

        Gallery::create($request->all());
        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('dashboard.galleries.index');

    }//end of store

    public function edit(Gallery $gallery)
    {
        return view('dashboard.galleries.edit', compact('gallery'));

    }//end of edit

    public function update(Request $request, Gallery $gallery)
    {
        $rules = [];

        foreach (config('translatable.locales') as $locale) {

            $rules += [$locale . '.*' => 'required'];
            $rules += [$locale . '.title' => [Rule::unique('gallery_translations', 'title')->ignore($gallery->id, 'gallery_id')]];

        } //end of for each

        $rules += [
            'status' => 'required',
            'comment_status' => 'required',
        ];

        $request->validate($rules);

        $request_data = $request->all();

        if ($request->image) {

            if ($gallery->image) {
                Storage::disk('public_uploads')->delete('gallery_images/' . $gallery->image);
            }

            $request->file('image')->store('gallery_images', 'public_uploads');
            $request_data['image'] = $request->image->hashName();

        }//end of if

        $gallery->update($request_data);
        session()->flash('success', __('site.updated_successfully'));
        return redirect()->route('dashboard.galleries.index');

    }//end fo update

    public function destroy(Gallery $gallery)
    {
        if ($gallery->gallery_images->count() > 0) {

            foreach ($gallery->gallery_images as $image) {

                Storage::disk('public_uploads')->delete('uploads/' . $image->name);

            }//end of for each

        }//end of if

        $gallery->delete();
        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->route('dashboard.galleries.index');

    }//end of destroy


}//end of gallery controller
