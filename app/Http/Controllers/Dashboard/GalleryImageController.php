<?php

namespace App\Http\Controllers\Dashboard;

use App\Gallery;
use App\Galleryimage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class GalleryImageController extends Controller
{
    public function store(Request $request)
    {
        switch ($request->type) {

            case 'gallery':
                $model = Gallery::FindOrFail($request->id);
                break;

        }//end fo switch

        $request->file('file')->store('/uploads', 'public_uploads');
        $image = $model->gallery_images()->create([
            'name' => $request->file->hashName(),
            'order' => $model->gallery_images->count()+1
        ]);

        return $image;

    }//end of store

    public function sort(Request $request)
    {
        foreach ($request->ids  as $index=>$id) {

            DB::table('gallery_images')->where('id', $id)->update(['order' => $index + 1]);

        }//end of for each

    }//end of sort

    public function update(Request $request)
    {
        DB::table('gallery_images')
            ->where('id', $request->id)
            ->update($request->except(['id', '_token', '_method']));

        return redirect()->back();

    }//end of update

    public function destroy(GalleryImage $gallery_image)
    {
        Storage::disk('public_uploads')->delete('/uploads/' . $gallery_image->name);
        $gallery_image->delete();
        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->back();

    }//end of destroy

}//end of controller
