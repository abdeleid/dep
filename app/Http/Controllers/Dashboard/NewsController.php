<?php

namespace App\Http\Controllers\Dashboard;

use App\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;

class NewsController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:read_news')->only('index');
        $this->middleware('permission:create_news')->only('create');
        $this->middleware('permission:update_news')->only('edit');
        $this->middleware('permission:delete_news')->only('destroy');

    }//end of constructor

    public function index(Request $request)
    {
        $news = News::where('department_id', null)->where(function($q) use ($request) {

            return $q->when($request->search, function($query) use ($request) {

                return $query->whereTranslationLike('title', '%' . $request->search . '%')
                    ->orWhereTranslationLike('content', '%' . $request->search . '%');

            });

        })->latest()->paginate(5);

        return view('dashboard.news.index', compact('news'));

    }//end of index

    public function create()
    {
        return view('dashboard.news.create');

    }//end of create

    public function store(Request $request)
    {
        $rules = [];

        foreach (config('translatable.locales') as $locale) {

            $rules += [$locale . '.*' => 'required'];
            $rules += [$locale . '.title' => ['required', Rule::unique('news_translations', 'title')]];

        }//end of for each

        $request->validate($rules);

        News::create($request->all());
        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('dashboard.news.index');

    }//end of store

    public function edit(News $news)
    {
        return view('dashboard.news.edit', compact('news'));

    }//end of edit

    public function update(Request $request, News $news)
    {
        $rules = [];

        foreach (config('translatable.locales') as $locale) {

            $rules += [$locale . '.*' => 'required'];
            $rules += [$locale . '.title' => ['required', Rule::unique('news_translations', 'title')->ignore($news->id, 'news_id')]];

        }//end of for each

        $request->validate($rules);

        $news->update($request->all());

        session()->flash('success', __('site.updated_successfully'));
        return redirect()->route('dashboard.news.index');

    }//end of update

    public function destroy(News $news)
    {
        $news->delete();
        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->route('dashboard.news.index');

    }//end of destroy

}//end of controller
