<?php

namespace App\Http\Controllers\Dashboard;

use App\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class PageController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:read_pages')->only('index');
        $this->middleware('permission:create_pages')->only('create');
        $this->middleware('permission:update_pages')->only('edit');
        $this->middleware('permission:update_pages')->only('remove_image');
        $this->middleware('permission:delete_pages')->only('destroy');

    }//end of constructor

    public function index(Request $request)
    {
        $pages = Page::where('department_id', null)->where(function ($q) use ($request) {

            return $q->when($request->search, function ($query) use ($request) {

                return $query->whereTranslationLike('title', $request->search);

            })->when($request->status, function($query) use ($request) {

                return $query->where('status', $request->status);

            });

        })->latest()->paginate(5);

        return view('dashboard.pages.index', compact('pages'));

    }//end of index

    public function create()
    {
        return view('dashboard.pages.create');

    }//end of create

    public function store(Request $request)
    {
        $rules = [];

        foreach (config('translatable.locales') as $locale) {

            $rules += [$locale . '.*' => 'required'];
            $rules += [$locale . '.title' => 'unique:page_translations,title'];

        } //end of for each

        $rules += [
            'status' => 'required',
            'comment_status' => 'required',
        ];

        $request->validate($rules);

        $request_data = $request->all();

        if ($request->image) {

            $request->file('image')->store('/uploads', 'public_uploads');
            $request_data['image'] = $request->image->hashName();

        }//end of if

        Page::create($request_data);
        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('dashboard.pages.index');

    }//end of store

    public function edit(Page $page)
    {
        return view('dashboard.pages.edit', compact('page'));

    }//end of edit

    public function update(Request $request, Page $page)
    {
        $rules = [];

        foreach (config('translatable.locales') as $locale) {

            $rules += [$locale . '.*' => 'required'];
            $rules += [$locale . '.title' => [Rule::unique('page_translations', 'title')->ignore($page->id, 'page_id')]];

        } //end of for each

        $rules += [
            'status' => 'required',
            'comment_status' => 'required',
        ];

        $request->validate($rules);

        $request_data = $request->all();

        if ($request->image) {

            if ($page->image) {
                Storage::disk('public_uploads')->delete('/uploads/' . $page->image);
            }

            $request->file('image')->store('/uploads', 'public_uploads');
            $request_data['image'] = $request->image->hashName();

        }//end of if

        $page->update($request_data);
        session()->flash('success', __('site.updated_successfully'));
        return redirect()->route('dashboard.pages.index');

    }//end fo update

    public function remove_image(Request $request, Page $page)
    {
        Storage::disk('public_uploads')->delete('/uploads/' . $page->image);

        $page->update([
            'image' => null
        ]);

        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('dashboard.pages.edit', $page->id);

    }//end of remove image

    public function destroy(Page $page)
    {
        if ($page->image) {

            Storage::disk('public_uploads')->delete('/uploads/' . $page->image);

        }//end of if

        $page->delete();
        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->route('dashboard.pages.index');

    }//end of destroy

}//end of controller
