<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SiteSettingController extends Controller
{
    public function create()
    {
        return view('dashboard.site_settings.create');

    }//end of create

    public function store(Request $request)
    {
        $request->validate([
            'site_name' => 'required',
        ]);

        setting($request->except(['_token', '_method']))->save();
        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('dashboard.site_settings.create');
        
    }//end of store

}//end of controller
