<?php

namespace App\Http\Controllers\Dashboard;

use App\Department;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $users = User::whereRoleIs('admin')->where(function ($query) use ($request) {

            return $query->when($request->search, function ($q) use ($request) {

                return $q->where('name', 'like', '%' . $request->search . '%')
                    ->orWhere('email', 'like', '%' . $request->search . '%');

            });

        })->latest()->paginate(5);

        return view('dashboard.users.index', compact('users'));

    }//end of index

    public function create()
    {
        $departments = Department::all();
        return view('dashboard.users.create', compact('departments'));

    }//end of create

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required|confirmed',
            'department_ids' => 'required',
        ]);

        $request_data = $request->except(['password', 'permissions', 'departments']);;
        $request_data['password'] = bcrypt($request->password);

        $user = User::create($request_data);

        $user->departments()->attach($request->department_ids);
        $user->attachRole('admin');
        $user->syncPermissions($request->permissions);

        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('dashboard.users.index');

    }//end of store

    public function edit(User $user)
    {
        $departments = Department::all();
        return view('dashboard.users.edit', compact('departments', 'user'));

    }//end of edit

    public function update(Request $request, User $user)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|unique:users,email,' . $user->id,
        ]);
        
        $user->update($request->all());
        $user->departments()->sync($request->department_ids);
        $user->syncPermissions($request->permissions);

        session()->flash('success', __('site.updated_successfully'));
        return redirect()->route('dashboard.users.index');

    }//end of update

    public function destroy(User $user)
    {
        $user->delete();
        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('dashboard.users.index');

    }//end of destroy

}//end of controller
