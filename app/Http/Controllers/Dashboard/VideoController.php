<?php

namespace App\Http\Controllers\Dashboard;

use App\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;

class VideoController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:read_pages')->only('index');
        $this->middleware('permission:create_pages')->only('create');
        $this->middleware('permission:update_pages')->only('edit');
        $this->middleware('permission:update_pages')->only('remove_image');
        $this->middleware('permission:delete_pages')->only('destroy');

    }//end of constructor

    public function index(Request $request)
    {
        $videos = Video::where('department_id', null)->where(function ($q) use ($request) {

            return $q->when($request->search, function ($query) use ($request) {

                return $query->whereTranslationLike('title', '%' . $request->search . '%');

            });

        })->latest()->paginate(5);

        return view('dashboard.videos.index', compact('videos'));

    }//end of index

    public function create()
    {
        return view('dashboard.videos.create');

    }//end of create

    public function store(Request $request)
    {
        $rules = [];

        foreach (config('translatable.locales') as $locale) {

            $rules += [$locale . '.*' => 'required'];
            $rules += [$locale . '.title' => 'unique:video_translations,title'];

        } //end of for each

        $rules += [
            'youtube_url' => 'required'
        ];

        $request->validate($rules);

        Video::create($request->all());
        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('dashboard.videos.index');

    }//end of store

    public function edit(Video $video)
    {
        return view('dashboard.videos.edit', compact('video'));

    }//end of edit

    public function update(Request $request, Video $video)
    {
        $rules = [];

        foreach (config('translatable.locales') as $locale) {

            $rules += [$locale . '.*' => 'required'];
            $rules += [$locale . '.title' => [Rule::unique('video_translations', 'title')->ignore($video->id, 'video_id')]];

        } //end of for each

        $request->validate($rules);

        $video->update($request->all());
        session()->flash('success', __('site.updated_successfully'));
        return redirect()->route('dashboard.videos.index');

    }//end of update

    public function destroy(Video $video)
    {
        $video->delete();
        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->route('dashboard.videos.index');

    }//end of destroy

}//end of controller
