<?php

namespace App\Http\Middleware;

use Closure;

class DepartmentMember
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->hasRole('super_admin')) {

            return $next($request);

        } else {

            $departments = auth()->user()->departments()->pluck('slug')->toArray();

            if (in_array(request()->department->slug, $departments)) {

                return $next($request);

            } else {

                abort(403, 'Unauthorized action.');

            }//end of else

        }//end of else

    }//end of handle

}//end of middleware
