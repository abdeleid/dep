<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use \Dimsav\Translatable\Translatable;

    protected $guarded = [];
    protected $appends = ['image_path'];
    public $translatedAttributes = ['title', 'content'];

    public function getImagePathAttribute()
    {
        return asset('uploads/' . $this->image);

    }//end of get image path

    public function department()
    {
        return $this->belongsTo(Department::class);

    }//end of department

}//end of model
