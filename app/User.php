<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;

class User extends Authenticatable
{
    use LaratrustUserTrait;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'google2fa_secret'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'google2fa_secret'
    ];

    public function setGoogle2faSecretAttribute($value)
    {
        $this->attributes['google2fa_secret'] = encrypt($value);

    }//end of set google 2fa

    public function getGoogle2faSecretAttribute($value)
    {
        return decrypt($value);

    }//end fo set google 2fa

    public function getNameAttribute($value)
    {
        return ucfirst($value);

    }//end of get name attribute

    public function departments()
    {
        return $this->belongsToMany(Department::class, 'user_department');

    }//end of department

}//end of model
