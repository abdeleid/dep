<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    use \Dimsav\Translatable\Translatable;

    protected $guarded = [];
    public $translatedAttributes = ['title'];

}//end of model
