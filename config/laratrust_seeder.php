<?php

return [
    'role_structure' => [
        'super_admin' => [
            'users' => 'c,r,u,d',
            'departments' => 'c,r,u,d',
            'pages' => 'c,r,u,d',
            'news' => 'c,r,u,d',
            'faqs' => 'c,r,u,d',
            'events' => 'c,r,u,d',
            'galleries' => 'c,r,u,d',
            'videos' => 'c,r,u,d',

            'department_departments' => 'c,r,u,d',
            'department_pages' => 'c,r,u,d',
            'department_faqs' => 'c,r,u,d',
            'department_events' => 'c,r,u,d',
            'department_news' => 'c,r,u,d',
            'department_galleries' => 'c,r,u,d',
            'department_videos' => 'c,r,u,d',
            'department_users' => 'c,r,u,d',
        ],

        'admin' => [

        ]
    ],

    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];
