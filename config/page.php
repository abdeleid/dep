<?php

return [

    'status' => [
        'disabled',
        'request_approve',
        'publish'
    ],

    'comment' => [
        'auto_publish',
        'pending_approve',
        'disabled'
    ],
];