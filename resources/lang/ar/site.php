<?php

return [
    'main' => 'الرئيسيه',
    'dashboard' => 'لوحه التحكم',
    'back_to_main' => 'الرجوع الى الرئيسيه',

    'logout' => 'تسجيل الخروج',

    'search' => 'بحث',
    'no_data_found' => 'للاسف لا يوجد اي سجلات',

    'create' => 'اضف',
    'edit' => 'تعديل',
    'update' => 'تعديل',
    'delete' => 'حذف',
    'show' => 'عرض',
    'read' => 'عرض',
    'action' => 'اكشن',

    'confirm_delete' => 'تاكيد الحذف',
    'yes' => 'نعم',
    'no' => 'لا',

    'added_successfully' => 'تم اضافه البيانات بنجاح',
    'updated_successfully' => 'تم تعديل البيانات بنجاح',
    'deleted_successfully' => 'تم حذف البيانات بنجاح',

    'departments' => 'الاقسام',
    'department_departments' => 'اقسام القسم',
    'department' => 'القسم',
    'all_departments' => 'كل الاقسام',
    'title' => 'العنوان',
    'attachments' => 'متعلقات',
    'departments_project' => 'مشروع الاقسام',

    'pages' => 'صفحات',
    'department_pages' => 'صفحات القسم',
    'image' => 'صوره',
    'status' => 'الحاله',
    'all_status' => 'كل الحالات',
    'comment_status' => 'حاله التعليقات',

    'page_status' => [
        'disabled' => 'غير متاح',
        'request_approve' => 'طلب مراجعه',
        'publish' => 'نشر',
    ],

    'page_comment' => [
        'auto_publish' => 'نشر تلقائى',
        'pending_approve' => 'انتظار الموافقه',
        'disabled' => 'غير متاح',
    ],

    'faqs' => 'الاسئله الشائعه',
    'department_faqs' => 'الاسئله الشائعه للقسم',

    'news' => 'الاخبار',
    'department_news' => 'اخبار القسم',
    'department_pages' => 'صفحات القسم',
    'events' => 'الاحداث',
    'department_events' => 'احداث القسم',
    'galleries' => 'البومات الصور',
    'department_galleries' => 'البومات الصور القسم',
    'gallery_images' => 'معرض الصور',
    'drop_images' => 'اختار او قم بسجب الصور هنا',
    'images_count' => 'عدد الصور',
    'add_images' => 'اضف صور',
    'close' => 'غلق',

    'videos' => 'الفيديوهات',
    'department_videos' => 'الفيديوهات القسم',
    'youtube_url' => 'رابط ال youtube',

    'ar' => [
        'language' => 'اللغه العربيه',
        'name' => 'الاسم بالغه العربيه',
        'title' => 'العنوان بالغه العربيه',
        'content' => 'المحتوى بالغه العربيه',

        'question' => 'السوال بالغه العربيه',
        'answer' => 'الاجابه بالغه العربيه',
    ],

    'en' => [
        'language' => 'اللغه الانجليزيه',
        'name' => 'الاسم بالغه الانجليزيه',
        'title' => 'العنوان بالغه الانجليزيه',
        'content' => 'المحتوى بالغه الانجليزيه',

        'question' => 'السوال بالغه الانجليزيه',
        'answer' => 'الاجابه بالغه الانجليزيه',
    ],

    'users' => 'المستخدمين',
    'department_users' => 'مستخدمين القسم',
    'name' => 'الاسم',
    'email' => 'البريد الاكتروني',
    'password' => 'كلمه المرور',
    'confirm_password' => 'تاكيد كلمه المرور',
    'permissions' => 'الصلاحيات',

    'site_settings' => 'اعدادات الموقع',
    'site_name' => 'اسم الموقع',
    'logo' => 'اللوجو',
];