@extends('layouts.dashboard.app')

@section('side_nav')
    @include('layouts.dashboard.departments._aside')
@endsection

@section('content')

    <div class="container-fluid">

        <h4 class="page-title">@lang('site.galleries')</h4>

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">@lang('site.dashboard')</a></li>
                <li class="breadcrumb-item"><a href="{{ route('dashboard.departments.galleries.index', $department->slug) }}">@lang('site.galleries')</a></li>
                <li class="breadcrumb-item active" aria-current="page">@lang('site.edit')</li>
            </ol>
        </nav>

        <div class="row">
            <div class="col-md-12">

                <div class="card">

                    <div class="card-body">

                        @include('dashboard.partials._errors')

                        <form action="{{ route('dashboard.departments.galleries.update', ['department' => $department->slug, 'gallery' => $gallery->id]) }}" method="post" enctype="multipart/form-data">

                            {{ csrf_field() }}
                            {{ method_field('put') }}

                            <ul class="nav nav-tabs" id="myTab" role="tablist">

                                @foreach (config('translatable.locales') as $index=>$locale)
                                    <li class="nav-item">
                                        <a class="nav-link {{ $index == 0 ? 'active' : ''}}" id="#{{ $locale }}" data-toggle="tab" href="#{{ $locale }}" role="tab" aria-controls="{{ $locale }}" aria-selected="true">
                                            @lang('site.' . $locale . '.language')
                                        </a>
                                    </li>
                                @endforeach

                            </ul><!-- end of nav tabs -->

                            <div class="tab-content" id="myTabContent">

                                @foreach (config('translatable.locales') as $index=>$locale)

                                    <div class="tab-pane fade show {{ $index == 0 ? 'active' : '' }}" id="{{ $locale }}" role="tabpanel" aria-labelledby="{{ $locale }}-tab">

                                        <div class="form-group">
                                            <label>@lang('site.' . $locale . '.title')</label>
                                            <input type="text" name="{{ $locale }}[title]" class="form-control" value="{{ $gallery->translate($locale)->title }}">
                                        </div>

                                    </div><!-- end of tab pane -->

                                @endforeach

                            </div><!-- end of tab content -->

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary"><i class="la la-plus"></i> @lang('site.edit')</button>
                            </div>

                        </form><!-- end of form -->

                        <hr>

                        <h6 class="box-title">@lang('site.gallery_images')</h6>

                        <form action="{{ route('dashboard.gallery_images.store') }}" id="dz" class="dropzone" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="fallback">
                                <input name="file" type="file" multiple/>
                            </div>
                        </form>

                        <div class="row" id="gallery-images" style="margin-top: 20px">
                            @foreach ($gallery->gallery_images as $image)
                                <div class="col-md-3" style="margin-top: 10px" id="{{ $image->id }}">
                                    <div class="gallery-image-container">
                                        <img src="{{ asset('uploads/' . $image->name ) }}" class="img-thumbnail" alt=""/>
                                        <div class="overlay">
                                            <form action="{{ route('dashboard.gallery_images.destroy', $image->id) }}" method="post" style="display: inline-block;">
                                                {{ csrf_field() }}
                                                {{ method_field('delete') }}
                                                <button type="submit" class="btn btn-danger btn-sm">@lang('site.delete')</button>
                                            </form>
                                            <button class="btn btn-info btn-sm" id="gallery-image-modal-btn" data-id="{{ $image->id }}" data-toggle="modal" data-target="#gallery-image-modal">@lang('site.edit')</button>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div><!-- end of box body -->

                    </div><!-- end of box -->

                </div><!-- end of card body -->

            </div><!-- end of card -->

        </div><!-- end of col 12 -->

    </div><!-- end of row -->

    </div><!-- end of container fluid -->
    
    @include('dashboard.galleries._modal')

@endsection

@push('scripts')

    <script>
        {{--drop zone--}}
        var myDropzone = new Dropzone("#dz", {
                dictDefaultMessage: "{{ __('site.drop_images') }}",
                params: {'id': '{{ $gallery->id }}', 'type': 'gallery'},
                complete: function (file) {
                    myDropzone.removeFile(file)
                },
                success: function (file, response) {

                    var lang = $(location).attr('href').split("/")[3];

                    var html = `
                    <div class="col-md-3" style="margin-top: 10px" id="${response.id}">
                        <div class="gallery-image-container">
                            <img src="/uploads/${response.name}" class="img-thumbnail" alt=""/>
                            <div class="overlay">
                                <form action="/${lang}/dashboard/gallery_images/${response.id}" method="post" style="display: inline-block">
                                    {{ csrf_field() }} {{ method_field('delete') }}
                                    <button type="submit" class="btn btn-danger btn-sm">{{ __('site.delete') }}</button>
                                </form>
                                <button class="btn btn-info btn-sm" id="gallery-image-modal-btn" data-id="${response.id}" data-toggle="modal" data-target="#gallery-image-modal">{{ __('site.edit') }}</button>
                            </div>
                        </div>
                    </div>
                    `;
                    $('#gallery-images').append(html);
                }
            });

        $('#gallery-images').sortable({
            tolerance: 'pointer',
            opacity: 0.60,
            cursor: "move",

            update: function (event, ui) {

                let url = "{{ route('dashboard.gallery_images.sort') }}";
                let ids = $(this).sortable('toArray');
                let data = {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'ids': ids
                }

                $.ajax({
                    url: url,
                    method: 'put',
                    data: data,
                    success: function (data) {
                        // $('#' + property).empty().append(data);
                    },
                })
            }
        })

        $('body').on('click', '#gallery-image-modal-btn', function() {

            var id = $(this).data('id');
            $('#gallery-image-id').val(id);

        })
    </script>

@endpush

