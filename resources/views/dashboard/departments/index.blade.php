@extends('layouts.dashboard.app')

@if (request()->parent)

    @section('side_nav')
        @include('layouts.dashboard.departments._aside')
    @endsection

@else

    @section('side_nav')
        @include('layouts.dashboard._aside')
    @endsection

@endif
@section('content')

    <div class="container-fluid">

        <h4 class="page-title mb-2">@lang('site.departments') <small>{{ $departments->total() }}</small></h4>

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">@lang('site.dashboard')</a></li>
                <li class="breadcrumb-item active" aria-current="page">@lang('site.departments')</li>
            </ol>
        </nav>

        <div class="row">
            <div class="col-md-12">

                <div class="card">

                    <div class="card-body pt-0">

                        <form action="{{ route('dashboard.departments.index') }}">
                            
                            <div class="row">
                                
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>@lang('site.search')</label>
                                        <input type="text" name="search" class="form-control" placeholder="@lang('site.search')" value="{{ request()->search }}">
                                    </div>

                                    @if (request()->parent)
                                        <input type="hidden" name="parent" value="{{ request()->parent }}">
                                    @endif

                                </div><!-- end of col -->
                                
                                <div class="col-md-4">
                                    
                                    <div class="form-group">
                                        <label class="d-block">@lang('site.search')</label>
                                        <button type="submit" class="btn btn-primary"><i class="la la-search"></i> @lang('site.search')</button>
                                        @if (request()->parent)

                                            @if (auth()->user()->hasPermission('create_department_departments'))
                                                <a href="{{ route('dashboard.departments.create', ['parent' => request()->parent]) }}" class="btn btn-primary"><i class="la la-plus"></i> @lang('site.create')</a>
                                            @else
                                                <a href="#" disabled class="btn btn-primary disabled"><i class="la la-plus"></i> @lang('site.create')</a>
                                            @endif

                                        @else

                                            @if (auth()->user()->hasPermission('create_departments'))
                                                <a href="{{ route('dashboard.departments.create') }}" class="btn btn-primary"><i class="la la-plus"></i> @lang('site.create')</a>
                                            @else
                                                <a href="#" disabled class="btn btn-primary disabled"><i class="la la-plus"></i> @lang('site.create')</a>
                                            @endif

                                        @endif

                                    </div>
                                    
                                </div><!-- end of col -->
                                
                            </div><!-- end of row -->
                            
                        </form><!-- end of form -->
                        @if ($departments->count() > 0)

                            <table class="table table-hover mt-2">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>@lang('site.title')</th>
                                    <th>@lang('site.action')</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach ($departments as $index=>$department)

                                    <tr>
                                        <td>{{ $index + 1 }}</td>
                                        <td>{{ $department->name }}</td>
                                        <td>
                                            <a href="{{ route('dashboard.departments.show', $department->slug) }}" class="btn btn-dark btn-sm">@lang('site.show')</a>
                                            @if (request()->parent)

                                                @if (auth()->user()->hasPermission('update_department_departments'))
                                                    <a href="{{ route('dashboard.departments.edit', ['department' => $department->slug, 'parent' => request()->parent]) }}" class="btn btn-info btn-sm"><i class="la la-edit"></i> @lang('site.edit')</a>
                                                @else
                                                    <a href="#" disabled class="btn btn-info btn-sm disabled"><i class="la la-edit"></i> @lang('site.edit')</a>
                                                @endif

                                            @else

                                                @if (auth()->user()->hasPermission('update_departments'))
                                                    <a href="{{ route('dashboard.departments.edit', $department->slug) }}" class="btn btn-info btn-sm"><i class="la la-edit"></i> @lang('site.edit')</a>
                                                @else
                                                    <a href="#" disabled class="btn btn-info btn-sm disabled"><i class="la la-edit"></i> @lang('site.edit')</a>
                                                @endif

                                            @endif

                                            @if (request()->parent)

                                                @if (auth()->user()->hasPermission('delete_department_departments'))
                                                    <form action="{{ route('dashboard.departments.destroy', $department->slug) }}" method="post" style="display: inline-block;">
                                                        {{ csrf_field() }}
                                                        {{ method_field('delete') }}
                                                        <button class="btn btn-danger btn-sm delete"><i class="la la-trash"></i> @lang('site.delete')</button>
                                                        <input type="hidden" name="parent" value="{{ request()->parent }}">
                                                    </form>
                                                @else
                                                    <button disabled class="btn btn-danger btn-sm delete disabled"><i class="la la-trash"></i> @lang('site.delete')</button>
                                                @endif

                                            @else

                                                @if (auth()->user()->hasPermission('delete_departments'))
                                                    <form action="{{ route('dashboard.departments.destroy', $department->slug) }}" method="post" style="display: inline-block;">
                                                        {{ csrf_field() }}
                                                        {{ method_field('delete') }}
                                                        <button class="btn btn-danger btn-sm delete"><i class="la la-trash"></i> @lang('site.delete')</button>
                                                    </form>
                                                @else
                                                    <button disabled class="btn btn-danger btn-sm delete disabled"><i class="la la-trash"></i> @lang('site.delete')</button>
                                                @endif

                                            @endif

                                        </td>

                                    </tr>

                                @endforeach

                                </tbody>

                            </table><!-- end of table -->

                            {{ $departments->appends(request()->query())->links() }}
                        @else
                            <h5 class="mt-4">@lang('site.no_data_found')</h5>
                        @endif
                        
                    </div><!-- end of card body -->
                    
                </div><!-- end of card -->

            </div><!-- end of col 12 -->
            
        </div><!-- end of row -->
        
    </div><!-- end of container fluid -->

@endsection