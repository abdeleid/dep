@extends('layouts.dashboard.app')

@section('side_nav')
    @include('layouts.dashboard.departments._aside')
@endsection

@section('content')

    <div class="container-fluid">

        <h4 class="faq-title mb-2">@lang('site.news')
            <small>{{ $news->total() }}</small>
        </h4>

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">@lang('site.dashboard')</a></li>
                <li class="breadcrumb-item"><a href="{{ route('dashboard.departments.index') }}">@lang('site.departments')</a></li>
                <li class="breadcrumb-item active">{{ $department->name }}</li>
                <li class="breadcrumb-item active" aria-current="faq">@lang('site.news')</li>
            </ol>
        </nav>

        <div class="row">

            <div class="col-md-12">

                <div class="card">

                    <div class="card-body pt-0">

                        <form action="{{ route('dashboard.departments.news.index', $department->slug) }}">

                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>@lang('site.search')</label>
                                        <input type="text" name="search" class="form-control" placeholder="@lang('site.search')" value="{{ request()->search }}">
                                    </div>
                                </div><!-- end of col -->

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="d-block">@lang('site.action')</label>
                                        <button type="submit" class="btn btn-primary"><i class="la la-search"></i> @lang('site.search')</button>
                                        @if (auth()->user()->hasPermission('create_department_news'))
                                            <a href="{{ route('dashboard.departments.news.create', $department->slug) }}" class="btn btn-primary"><i class="la la-plus"></i> @lang('site.create')</a>
                                        @else
                                            <a href="#" class="btn btn-primary disabled"><i class="la la-plus"></i> @lang('site.create')</a>
                                        @endif
                                    </div>

                                </div><!-- end of col -->

                            </div><!-- end of row -->

                        </form><!-- end of form -->

                        @if ($news->count() > 0)

                            <table class="table table-hover mt-2">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>@lang('site.title')</th>
                                    <th>@lang('site.action')</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach ($news as $index=>$n)

                                    <tr>
                                        <td>{{ $index + 1 }}</td>
                                        <td>{{ $n->title }}</td>
                                        <td>
                                            @if (auth()->user()->hasPermission('update_department_news'))
                                                <a href="{{ route('dashboard.departments.news.edit', ['department' => $department->slug, 'faq' => $n->id]) }}" class="btn btn-info btn-sm"><i class="la la-edit"></i> @lang('site.edit')</a>
                                            @else
                                                <a href="#" class="btn btn-info btn-sm disabled"><i class="la la-edit"></i> @lang('site.edit')</a>
                                            @endif

                                            @if (auth()->user()->hasPermission('delete_department_news'))
                                                <form action="{{ route('dashboard.departments.news.destroy', ['department' => $department->slug, 'faq' => $n->id]) }}" method="post" style="display: inline-block;">
                                                    {{ csrf_field() }}
                                                    {{ method_field('delete') }}
                                                    <button class="btn btn-danger btn-sm delete"><i class="la la-trash"></i> @lang('site.delete')</button>
                                                </form>
                                            @else
                                                <button class="btn btn-danger btn-sm disabled"><i class="la la-trash"></i> @lang('site.delete')</button>
                                            @endif

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table><!-- end of table -->

                            {{ $news->appends(request()->query())->links() }}
                        @else
                            <h5 class="mt-4">@lang('site.no_data_found')</h5>
                        @endif

                    </div><!-- end of card body -->

                </div><!-- end of card -->

            </div><!-- end of col 12 -->

        </div><!-- end of row -->

    </div><!-- end of container fluid -->

@endsection