@extends('layouts.dashboard.app')

@section('side_nav')
    @include('layouts.dashboard._aside')
@endsection


@section('content')

    <div class="container-fluid">

        <h4 class="event-title">@lang('site.events')</h4>

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">@lang('site.dashboard')</a></li>
                <li class="breadcrumb-item"><a href="{{ route('dashboard.events.index') }}">@lang('site.events')</a></li>
                <li class="breadcrumb-item active" aria-current="event">@lang('site.edit')</li>
            </ol>
        </nav>

        <div class="row">
            <div class="col-md-12">

                <div class="card">

                    <div class="card-body">

                        @include('dashboard.partials._errors')

                        <form action="{{ route('dashboard.events.update', $event->id) }}" method="post" enctype="multipart/form-data">

                            {{ csrf_field() }}
                            {{ method_field('put') }}

                            <ul class="nav nav-tabs" id="myTab" role="tablist">

                                @foreach (config('translatable.locales') as $index=>$locale)
                                    <li class="nav-item">
                                        <a class="nav-link {{ $index == 0 ? 'active' : ''}}" id="#{{ $locale }}" data-toggle="tab" href="#{{ $locale }}" role="tab" aria-controls="{{ $locale }}" aria-selected="true">
                                            @lang('site.' . $locale . '.language')
                                        </a>
                                    </li>
                                @endforeach

                            </ul><!-- end of nav tabs -->

                            <div class="tab-content" id="myTabContent">

                                @foreach (config('translatable.locales') as $index=>$locale)

                                    <div class="tab-pane fade show {{ $index == 0 ? 'active' : '' }}" id="{{ $locale }}" role="tabpanel" aria-labelledby="{{ $locale }}-tab">

                                        <div class="form-group">
                                            <label>@lang('site.' . $locale . '.title')</label>
                                            <input type="text" name="{{ $locale }}[title]" class="form-control" value="{{ $event->translate($locale)->title }}">
                                        </div>

                                        <div class="form-group">
                                            <label>@lang('site.' . $locale . '.content')</label>
                                            <textarea name="{{ $locale }}[content]" class="form-control ckeditor" id="ckeditor-{{ $locale }}">{{ $event->translate($locale)->content }}</textarea>
                                        </div>

                                    </div><!-- end of tab pane -->

                                @endforeach

                            </div><!-- end of tab content -->

                            <div class="form-group">
                                <label>@lang('site.image')</label>
                                <input type="file" name="image" class="form-control">
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary"><i class="la la-plus"></i> @lang('site.edit')</button>
                            </div>

                        </form><!-- end of form -->

                    </div><!-- end of card body -->

                </div><!-- end of card -->

            </div><!-- end of col 12 -->

        </div><!-- end of row -->

    </div><!-- end of container fluid -->

@endsection