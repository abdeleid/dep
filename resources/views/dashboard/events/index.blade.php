@extends('layouts.dashboard.app')

@section('side_nav')
    @include('layouts.dashboard._aside')
@endsection


@section('content')

    <div class="container-fluid">

        <h4 class="event-title mb-2">@lang('site.events') <small>{{ $events->total() }}</small></h4>

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">@lang('site.dashboard')</a></li>
                <li class="breadcrumb-item"><a href="{{ route('dashboard.events.index') }}">@lang('site.events')</a></li>
                <li class="breadcrumb-item active" aria-current="event">@lang('site.events')</li>
            </ol>
        </nav>

        <div class="row">

            <div class="col-md-12">

                <div class="card">

                    <div class="card-body pt-0">

                        <form action="{{ route('dashboard.events.index') }}">
                            
                            <div class="row">
                                
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>@lang('site.search')</label>
                                        <input type="text" name="search" class="form-control" placeholder="@lang('site.search')" value="{{ request()->search }}">
                                    </div>
                                </div><!-- end of col -->
                                
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="d-block">@lang('site.action')</label>
                                        <button type="submit" class="btn btn-primary"><i class="la la-search"></i> @lang('site.search')</button>
                                        <a href="{{ route('dashboard.events.create') }}" class="btn btn-primary"><i class="la la-plus"></i> @lang('site.create')</a>
                                    </div>
                                    
                                </div><!-- end of col -->
                                
                            </div><!-- end of row -->
                            
                        </form><!-- end of form -->

                        @if ($events->count() > 0)

                            <table class="table table-hover mt-2">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>@lang('site.title')</th>
                                    <th>@lang('site.image')</th>
                                    <th>@lang('site.action')</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach ($events as $index=>$event)

                                    <tr>
                                        <td>{{ $index + 1 }}</td>
                                        <td>{{ $event->title }}</td>
                                        <td><img src="{{ $event->image_path }}" class="img-thumbnail" style="width: 100px"></td>
                                        <td>
                                            <a href="{{ route('dashboard.events.edit', $event->id) }}" class="btn btn-info btn-sm"><i class="la la-edit"></i> @lang('site.edit')</a>
                                            <form action="{{ route('dashboard.events.destroy', $event->id) }}" method="post" style="display: inline-block;">
                                                {{ csrf_field() }}
                                                {{ method_field('delete') }}
                                                <button class="btn btn-danger btn-sm delete"><i class="la la-trash"></i> @lang('site.delete')</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table><!-- end of table -->

                            {{ $events->appends(request()->query())->links() }}
                        @else
                            <h5 class="mt-4">@lang('site.no_data_found')</h5>
                        @endif
                        
                    </div><!-- end of card body -->
                    
                </div><!-- end of card -->

            </div><!-- end of col 12 -->
            
        </div><!-- end of row -->
        
    </div><!-- end of container fluid -->

@endsection