@extends('layouts.dashboard.app')

@section('side_nav')
    @include('layouts.dashboard._aside')
@endsection

@section('content')

    <div class="container-fluid">

        <h4 class="faq-title">@lang('site.faqs')</h4>

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">@lang('site.dashboard')</a></li>
                <li class="breadcrumb-item"><a href="{{ route('dashboard.faqs.index') }}">@lang('site.faqs')</a></li>
                <li class="breadcrumb-item active" aria-current="faq">@lang('site.edit')</li>
            </ol>
        </nav>

        <div class="row">
            <div class="col-md-12">

                <div class="card">

                    <div class="card-body">

                        @include('dashboard.partials._errors')

                        <form action="{{ route('dashboard.faqs.update', $faq->id) }}" method="post" enctype="multipart/form-data">

                            {{ csrf_field() }}
                            {{ method_field('put') }}

                            <ul class="nav nav-tabs" id="myTab" role="tablist">

                                @foreach (config('translatable.locales') as $index=>$locale)
                                    <li class="nav-item">
                                        <a class="nav-link {{ $index == 0 ? 'active' : ''}}" id="#{{ $locale }}" data-toggle="tab" href="#{{ $locale }}" role="tab" aria-controls="{{ $locale }}" aria-selected="true">
                                            @lang('site.' . $locale . '.language')
                                        </a>
                                    </li>
                                @endforeach

                            </ul><!-- end of nav tabs -->

                            <div class="tab-content" id="myTabContent">

                                @foreach (config('translatable.locales') as $index=>$locale)

                                    <div class="tab-pane fade show {{ $index == 0 ? 'active' : '' }}" id="{{ $locale }}" role="tabpanel" aria-labelledby="{{ $locale }}-tab">

                                        <div class="form-group">
                                            <label>@lang('site.' . $locale . '.question')</label>
                                            <input type="text" name="{{ $locale }}[question]" class="form-control" value="{{ $faq->translate($locale)->question }}">
                                        </div>

                                        <div class="form-group">
                                            <label>@lang('site.' . $locale . '.answer')</label>
                                            <textarea name="{{ $locale }}[answer]" class="form-control ckeditor" id="ckeditor-{{ $locale }}">{{ $faq->translate($locale)->answer }}</textarea>
                                        </div>

                                    </div><!-- end of tab pane -->

                                @endforeach

                            </div><!-- end of tab content -->

                            <div class="form-group">
                                <label>@lang('site.image')</label>
                                <input type="file" name="image" class="form-control">
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary"><i class="la la-edit"></i> @lang('site.edit')</button>
                            </div>

                        </form><!-- end of form -->

                    </div><!-- end of card body -->

                </div><!-- end of card -->

            </div><!-- end of col 12 -->

        </div><!-- end of row -->

    </div><!-- end of container fluid -->

@endsection