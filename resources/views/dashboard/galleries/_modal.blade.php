<!-- Modal -->
<div class="modal fade" id="gallery-image-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">@lang('site.edit')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('dashboard.gallery_images.update') }}" method="post">
                    {{ csrf_field() }}
                    {{ method_field('put') }}

                    <div class="form-group">
                        <label>@lang('site.title')</label>
                        <input type="text" name="title" class="form-control" >
                        <input type="hidden" id="gallery-image-id" name="id" value="">
                    </div>

                    <button type="submit" class="btn btn-primary">@lang('site.edit')</button>

                </form><!-- end of form -->
            </div>
        </div>
    </div>
</div>