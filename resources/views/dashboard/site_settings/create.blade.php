@extends('layouts.dashboard.app')

@section('side_nav')
    @include('layouts.dashboard._aside')
@endsection

@section('content')

    <div class="container-fluid">

        <h4 class="page-title">@lang('site.site_settings')</h4>

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">@lang('site.dashboard')</a></li>
                <li class="breadcrumb-item active" aria-current="page">@lang('site.site_settings')</li>
            </ol>
        </nav>

        <div class="row">
            <div class="col-md-12">

                <div class="card">

                    <div class="card-body">

                        @include('dashboard.partials._errors')

                        <form action="{{ route('dashboard.site_settings.store') }}" method="post" enctype="multipart/form-data">

                            {{ csrf_field() }}
                            {{ method_field('post') }}

                            <div class="form-group">
                                <label>@lang('site.site_name')</label>
                                <input type="text" name="site_name" class="form-control" value="{{ setting('site_name') }}">
                            </div>

                            <div class="form-group">
                                <label>@lang('site.logo')</label>
                                <input type="file" name="logo" class="form-control">
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary"><i class="la la-plus"></i> @lang('site.create')</button>
                            </div>

                        </form><!-- end of form -->

                    </div><!-- end of card body -->

                </div><!-- end of card -->

            </div><!-- end of col 12 -->

        </div><!-- end of row -->

    </div><!-- end of container fluid -->

@endsection