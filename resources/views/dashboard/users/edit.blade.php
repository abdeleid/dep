@extends('layouts.dashboard.app')

@section('side_nav')
    @include('layouts.dashboard._aside')
@endsection

@section('content')

    <div class="container-fluid">

        <h4 class="user-title">@lang('site.users')</h4>

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">@lang('site.dashboard')</a></li>
                <li class="breadcrumb-item"><a href="{{ route('dashboard.users.index') }}">@lang('site.users')</a></li>
                <li class="breadcrumb-item active" aria-current="user">@lang('site.edit')</li>
            </ol>
        </nav>

        <div class="row">
            <div class="col-md-12">

                <div class="card">

                    <div class="card-body">

                        @include('dashboard.partials._errors')

                        <form action="{{ route('dashboard.users.update', $user->id) }}" method="post" enctype="multipart/form-data">

                            {{ csrf_field() }}
                            {{ method_field('put') }}

                            <div class="form-group">
                                <label>@lang('site.name')</label>
                                <input type="text" name="name" class="form-control" value="{{ $user->name }}">
                            </div>

                            <div class="form-group">
                                <label>@lang('site.name')</label>
                                <input type="email" name="email" class="form-control" value="{{ $user->email }}">
                            </div>

                            <div class="form-group">
                                <label>@lang('site.department')</label>
                                <select name="department_ids[]" class="form-control select2" multiple>
                                    @foreach ($departments as $department)
                                        <option
                                            value="{{ $department->id }}"
                                            {{ in_array($department->id, $user->departments->pluck('id')->toArray()) ? 'selected' : '' }}
                                        >
                                            {{ $department->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <h5 class="mb-3">@lang('site.permissions')</h5>
                                @php
                                    $models = ['departments', 'pages','users']
                                @endphp

                                <ul class="nav nav-tabs" id="myTab" role="tablist">

                                    @foreach ($models as $index=>$model)
                                        <li class="nav-item">
                                            <a class="nav-link {{ $index == 0 ? 'active' : ''}}" id="#{{ $model }}" data-toggle="tab" href="#{{ $model }}" role="tab" aria-controls="{{ $model }}" aria-selected="true">
                                                @lang('site.' . $model)
                                            </a>
                                        </li>
                                    @endforeach

                                </ul><!-- end of nav tabs -->
                                
                                <div class="tab-content" id="myTabContent">

                                    @foreach ($models as $index=>$model)

                                        @php
                                            $permissions = ['create', 'read', 'update', 'delete'];
                                        @endphp

                                        <div class="tab-pane fade show {{ $index == 0 ? 'active' : '' }}" id="{{ $model }}" role="tabpanel" aria-labelledby="{{ $model }}-tab">

                                            @foreach ($permissions as $permission)

                                                <div class="form-check mt-2" style="display: inline-block;">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" name="permissions[]" type="checkbox" {{ $user->hasPermission($permission . '_' . $model) ? 'checked' : '' }} value="{{ $permission . '_' . $model }}">
                                                        <span class="form-check-sign">@lang('site.' . $permission)</span>
                                                    </label>
                                                </div>

                                            @endforeach

                                        </div><!-- end of tab pane -->

                                    @endforeach

                                </div><!-- end of tab content -->

                                <br>

                                @php
                                    $models = ['department_departments', 'department_pages', 'department_faqs', 'department_news', 'department_events', 'department_users']
                                @endphp

                                <ul class="nav nav-tabs" id="myTab" role="tablist">

                                    @foreach ($models as $index=>$model)
                                        <li class="nav-item">
                                            <a class="nav-link {{ $index == 0 ? 'active' : ''}}" id="#{{ $model }}" data-toggle="tab" href="#{{ $model }}" role="tab" aria-controls="{{ $model }}" aria-selected="true">
                                                @lang('site.' . $model)
                                            </a>
                                        </li>
                                    @endforeach

                                </ul><!-- end of nav tabs -->

                                <div class="tab-content" id="myTabContent">

                                    @foreach ($models as $index=>$model)

                                        @php
                                            $permissions = ['create', 'read', 'update', 'delete'];
                                        @endphp

                                        <div class="tab-pane fade show {{ $index == 0 ? 'active' : '' }}" id="{{ $model }}" role="tabpanel" aria-labelledby="{{ $model }}-tab">

                                            @foreach ($permissions as $permission)

                                                <div class="form-check mt-2" style="display: inline-block;">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" name="permissions[]" type="checkbox" {{ $user->hasPermission($permission . '_' . $model) ? 'checked' : '' }} value="{{ $permission . '_' . $model }}">
                                                        <span class="form-check-sign">@lang('site.' . $permission)</span>
                                                    </label>
                                                </div>

                                            @endforeach

                                        </div><!-- end of tab pane -->

                                    @endforeach

                                </div><!-- end of tab content -->

                            </div>
                            
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary"><i class="la la-plus"></i> @lang('site.edit')</button>
                            </div>

                        </form><!-- end of form -->

                    </div><!-- end of card body -->

                </div><!-- end of card -->

            </div><!-- end of col 12 -->

        </div><!-- end of row -->

    </div><!-- end of container fluid -->

@endsection