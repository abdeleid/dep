<div class="sidebar">

    {{--<div class="user">--}}

    {{--<div class="photo"><img src="{{ asset('dashboard/img/profile.jpg') }}"></div>--}}

    {{--<div class="info">--}}
    {{--<a class="" data-toggle="collapse" href="#collapseExample" aria-expanded="true">--}}
    {{--<span>--}}
    {{--Hizrian--}}
    {{--<span class="user-level">Administrator</span>--}}
    {{--<span class="caret"></span>--}}
    {{--</span>--}}
    {{--</a>--}}
    {{--<div class="clearfix"></div>--}}

    {{--<div class="collapse in" id="collapseExample" aria-expanded="true" style="">--}}
    {{--<ul class="nav">--}}
    {{--<li>--}}
    {{--<a href="#profile">--}}
    {{--<span class="link-collapse">My Profile</span>--}}
    {{--</a>--}}
    {{--</li>--}}
    {{--<li>--}}
    {{--<a href="#edit">--}}
    {{--<span class="link-collapse">Edit Profile</span>--}}
    {{--</a>--}}
    {{--</li>--}}
    {{--<li>--}}
    {{--<a href="#settings">--}}
    {{--<span class="link-collapse">Settings</span>--}}
    {{--</a>--}}
    {{--</li>--}}
    {{--</ul>--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--</div><!-- end of user  -->--}}

    <ul class="nav">

        <li class="nav-item"><a href="{{ route('dashboard.index') }}"><i class="la la-dashboard"></i>@lang('site.main')</a></li>

        @if (auth()->user()->hasPermission('read_departments'))
            <li class="nav-item"><a href="{{ route('dashboard.departments.index') }}"><i class="la la-support"></i>@lang('site.departments')</a></li>
        @endif

        @if (auth()->user()->hasPermission('read_pages'))
            <li class="nav-item"><a href="{{ route('dashboard.pages.index') }}"><i class="la la-file"></i>@lang('site.pages')</a></li>
        @endif

        @if (auth()->user()->hasPermission('read_faqs'))
            <li class="nav-item"><a href="{{ route('dashboard.faqs.index') }}"><i class="la la-file"></i>@lang('site.faqs')</a></li>
        @endif

        @if (auth()->user()->hasPermission('read_events'))
            <li class="nav-item"><a href="{{ route('dashboard.events.index') }}"><i class="la la-file"></i>@lang('site.events')</a></li>
        @endif

        @if (auth()->user()->hasPermission('read_news'))
            <li class="nav-item"><a href="{{ route('dashboard.news.index') }}"><i class="la la-file"></i>@lang('site.news')</a></li>
        @endif

        @if (auth()->user()->hasPermission('read_galleries'))
            <li class="nav-item"><a href="{{ route('dashboard.galleries.index') }}"><i class="la la-file"></i>@lang('site.galleries')</a></li>
        @endif

        @if (auth()->user()->hasPermission('read_videos'))
            <li class="nav-item"><a href="{{ route('dashboard.videos.index') }}"><i class="la la-file"></i>@lang('site.videos')</a></li>
        @endif

        @if (auth()->user()->hasPermission('read_users'))
            <li class="nav-item"><a href="{{ route('dashboard.users.index') }}"><i class="la la-users"></i>@lang('site.users')</a></li>
        @endif

        @if (auth()->user()->hasRole('super_admin'))
            <li class="nav-item"><a href="{{ route('dashboard.site_settings.create') }}"><i class="la la-wrench"></i>@lang('site.site_settings')</a></li>
        @endif

    </ul><!-- end of nav -->

</div><!-- end of side bar -->