<div class="sidebar">

    <ul class="nav">

        @if (auth()->user()->hasPermission('read_department_departments'))
            <li class="nav-item"><a href="{{ route('dashboard.departments.index', ['parent' => $department->slug ?? request()->parent ]) }}"><i class="la la-support"></i>@lang('site.department_departments')</a></li>
        @endif

        @if (auth()->user()->hasPermission('read_department_pages'))
            <li class="nav-item"><a href="{{ route('dashboard.departments.pages.index', $department->slug ?? request()->parent ) }}"><i class="la la-support"></i>@lang('site.department_pages')</a></li>
        @endif

        @if (auth()->user()->hasPermission('read_department_faqs'))
            <li class="nav-item"><a href="{{ route('dashboard.departments.faqs.index', $department->slug ?? request()->parent ) }}"><i class="la la-file"></i>@lang('site.department_faqs')</a></li>
        @endif

        @if (auth()->user()->hasPermission('read_department_news'))
            <li class="nav-item"><a href="{{ route('dashboard.departments.news.index', $department->slug ?? request()->parent ) }}"><i class="la la-users"></i>@lang('site.department_news')</a></li>
        @endif

        @if (auth()->user()->hasPermission('read_department_events'))
            <li class="nav-item"><a href="{{ route('dashboard.departments.events.index', $department->slug ?? request()->parent ) }}"><i class="la la-wrench"></i>@lang('site.department_events')</a></li>
        @endif

        @if (auth()->user()->hasPermission('read_department_galleries'))
            <li class="nav-item"><a href="{{ route('dashboard.departments.galleries.index', $department->slug ?? request()->parent ) }}"><i class="la la-wrench"></i>@lang('site.department_galleries')</a></li>
        @endif

        @if (auth()->user()->hasPermission('read_department_videos'))
            <li class="nav-item"><a href="{{ route('dashboard.departments.videos.index', $department->slug ?? request()->parent ) }}"><i class="la la-wrench"></i>@lang('site.department_videos')</a></li>
        @endif

        @if (auth()->user()->hasPermission('read_department_users'))
            <li class="nav-item"><a href="{{ route('dashboard.departments.users.index', $department->slug ?? request()->parent ) }}"><i class="la la-wrench"></i>@lang('site.department_users')</a></li>
        @endif

        <li class="nav-item"><a href="{{ route('dashboard.index', $department->slug ?? request()->parent ) }}"><i class="la la-backward"></i>@lang('site.back_to_main')</a></li>

    </ul><!-- end of nav -->

</div><!-- end of side bar -->