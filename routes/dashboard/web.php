<?php

Route::prefix(LaravelLocalization::setLocale())
    ->middleware(['localeSessionRedirect', 'localizationRedirect', 'localeViewPath', 'auth', 'role:super_admin|admin'])
    ->group(function() {

    Route::prefix('dashboard')->name('dashboard.')->group(function () {

        Route::get('/index', 'DashboardController@index')->name('index');

        Route::namespace('Department')->group(function(){

            //page routes
            Route::put('departments/{department}/pages/{page}/remove_image', 'PageController@remove_image')->name('pages.remove_image');
            Route::resource('departments.pages', 'PageController');

            //faq routes
            Route::resource('departments.faqs', 'FaqController');

            //news routes
            Route::resource('departments.news', 'NewsController');

            //event routes
            Route::resource('departments.events', 'EventController');

            //gallery routes
            Route::resource('departments.galleries', 'GalleryController');

            //video routes
            Route::resource('departments.videos', 'VideoController');

            //user routes
            Route::resource('departments.users', 'UserController');

        });//end of department namespace

        //department routes
        Route::resource('departments', 'DepartmentController');

        //faq routes
        Route::resource('faqs', 'FaqController');

        //page routes
        Route::put('pages/{page}/remove_image', 'PageController@remove_image')->name('pages.remove_image');
        Route::resource('pages', 'PageController');

        //event routes
        Route::resource('events', 'EventController');

        //news routes
        Route::resource('news', 'NewsController');

        //galleries routes
        Route::resource('galleries', 'GalleryController');

        //user routes
        Route::resource('users', 'UserController');

        //site setting routes
        Route::resource('site_settings', 'SiteSettingController')->only(['create', 'store']);

        //gallery images
        Route::put('/gallery_images/sort', 'GalleryImageController@sort')->name('gallery_images.sort');
        Route::put('/gallery_images/update', 'GalleryImageController@update')->name('gallery_images.update');
        Route::resource('gallery_images', 'GalleryImageController')->only(['store', 'destroy']);

        //video routes
        Route::resource('videos', 'VideoController');

    });

});
